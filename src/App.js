import React, { useState } from 'react';
import { createGlobalStyle } from 'styled-components';
import Futura from './assets/futura/futura_medium_bt.ttf'
import FuturaLight from './assets/futura/futura_light_bt.ttf'

import SideBar from './components/sideBar/SideBar'
import MainContainer from './components/main/MainCointainer';
import { Route } from 'react-router-dom';

const GlobalStyle = createGlobalStyle`
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

/* Set every element's box-sizing to border-box */
* {
    box-sizing: border-box;
}
@font-face {
	font-family: 'Futura';
	font-style: normal;
	font-weight: 400px;
	src:
		local('Futura'),
		url(${Futura}) format('truetype')
}
@font-face {
	font-family: 'Futura-Light';
	font-style: normal;
	font-weight: 400px;
	src:
		local('Futura'),
		url(${FuturaLight}) format('truetype')
}
html, body {
  font-size: 62.5%;
	margin: 0 auto;
  background-color: #cad9ff;
	font-family: 'Futura'
}
a{
	text-decoration:none;
}
strong {
        font-weight: bold;
    }
p, td{
	font-family: 'Futura-Light'
}
`


function App() {
  const [menuShown, setMenu] = useState(false)

  const toogleMenu = () => {
    setMenu(!menuShown);
  }

  return (
    <>
      <GlobalStyle />
      <SideBar menuShown={menuShown} toogleMenu={toogleMenu} />
      <Route exact path='/' render={props => <MainContainer toogleMenu={toogleMenu} menuShown={menuShown} {...props} />} />
    </>
  );
}

export default App;

