import reactIcon from '../assets/reactjs-icon.svg'
import reduxIcon from '../assets/redux-logo.png'
import nextIcon from '../assets/next_logo.svg'
import styledIcon from '../assets/styled-components-1.svg'

const frontend = [
  {
    icon: reactIcon,
    name: 'React.js'
  },
  {
    icon: reduxIcon,
    name: 'Redux.js'
  },
  {
    icon: nextIcon,
    name: 'Next.js'
  },
  {
    icon: styledIcon,
    name: 'Styled Components'
  }
]

export default frontend