import nodeIcon from '../assets/nodejs-icon.svg'
import expressIcon from '../assets/express.svg'
import postgreIcon from '../assets/postgresql-icon.svg'
import mongoDbIcon from '../assets/mongodb.svg'
const backend = [
    {
        icon: nodeIcon,
        name: 'Node.js'
    },
    {
        icon: expressIcon,
        name: 'Express.js'
    },
    {
        icon: postgreIcon,
        name: 'PostgreSQL'
    },
    {
        icon: mongoDbIcon,
        name: 'MongoDb'
    },
]

export default backend