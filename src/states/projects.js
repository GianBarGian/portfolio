import goBoard from '../assets/goBoard.png'
import yogicYantra from '../assets/yogicYantraScreenshot.png'
import agmf from '../assets/agmfScreenshot.png'
import dapp from '../assets/dappImage.png'

const projects = [
  {
    image: yogicYantra,
    imgAlt: "Yogic Yantra",
    title: "Yogic Yantra",
    description: "This has been the project I <strong>developed and maintained</strong> in the last few years. It's an app developed using <strong>React and Redux</strong> to build a SPA front-end and a <strong>Wordpress instance as an Headless CMS</strong> as a back-end. It works exposing a <strong>REST API</strong> then used to fetch data from the front-end. <br /> It consists as should be of many moving parts and different logic used to give the end user a smooth experience. <br /> In order to develop the app as it stands now I used <strong>many different tools, libraries, and technologies</strong> such as <strong>Python</strong> to develop various <strong>script to automate processes</strong> in the back-end.",
    repo: 'https://gitlab.com/GianBarGian/yogic-yantra'
  },
  {
    image: agmf,
    imgAlt: "AGMF",
    title: "AGMF",
    description: "This is another small project I created for a client that needed a brochure website, with a small contact form.<br /> While this project is not very complex logic side, it showcase <strong>my experience with UX/UI</strong> and <strong>design with marketing in mind</strong> on the front-end. While I understand it's not an important skill to have, I believe is always useful for someone working on the front-end to have a <strong>clear understanding</strong> how and why a thing should be designed and built the way it is.<br />Moreover this project deal with the logic behind <strong>multi-language website</strong>.",
    repo: 'https://gitlab.com/GianBarGian/agmf'
  },
  {
    image: dapp,
    imgAlt: "Dapp Logo",
    title: "D-app template",
    description: "This is about the new direction I'm moving my career to. It's a <strong>full stack decentralised application template</strong>, ready to be used to build on the <strong>Ethereum Ecosystem</strong>. <br /> It is composed by a <strong>Truffle environment</strong> for developing smart contracts, a traditional back-end in <strong>Node.js/Express.js</strong> using <strong>MongoDb</strong> as a database, and a <strong>Next.js/Redux.js</strong> front-end.",
    repo: 'https://gitlab.com/GianBarGian/full_stack_etherum_dapp_template'
  },
  {
    image: goBoard,
    imgAlt: "Go board",
    title: "Go game",
    description: "This project is all about <strong>logic and functional programming</strong>. It's a simple app that permit a user to play a Go game. <br />The cool part about this project is that it's all built with <strong>functional programming</strong> in mind. This means -on top of other things- that every <strong>variable is constant</strong>, there is <strong>no reassigning</strong> inside objects and arrays, and every function in written <strong>recursively</strong>. <br /> This project is all about <strong>coding logic</strong>!",
    repo: 'https://gitlab.com/GianBarGian/goban'
  },

];
export default projects;