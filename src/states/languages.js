import jsIcon from '../assets/javascript-icon.svg'
import pyIcon from '../assets/python-icon.svg'
import sqlIcon from '../assets/database.svg'
import solIcon from '../assets/solidity_logo.svg'

const languages = [
  {
    icon: jsIcon,
    name: 'Javascript'
  },
  {
    icon: pyIcon,
    name: 'Python'
  },
  {
    icon: sqlIcon,
    name: 'SQL'
  },
  {
    icon: solIcon,
    name: 'Solidity'
  }
]

export default languages