import linuxIcon from '../assets/linux-icon.svg'
import gitIcon from '../assets/git-logo.png'
import agileIcon from '../assets/agile-logo.png'
import polyaIcon from '../assets/idea.png'

const otherSkills = [
    {
        icon: polyaIcon,
        name: "Polya's problem solving thecnique"
    },
    {
        icon: gitIcon,
        name: 'Git'
    },
    {
        icon: agileIcon,
        name: 'Agile Methodology'
    },
    {
        icon: linuxIcon,
        name: 'Linux'
    },
]

export default otherSkills