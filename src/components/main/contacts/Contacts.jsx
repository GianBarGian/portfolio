import React from "react";
import styled from "styled-components";
import { animateScroll as scroll } from "react-scroll";

import phone from "../../../assets/phone.png";
import mail from "../../../assets/mail.png";

const StyledContacts = styled.section`
  margin-top: 80px;
  border-top: 1px solid gray;
  @media (max-width: 550px) {
    border-top: none;
  }
  h2 {
    @media (max-width: 640px) {
        padding: 20px !important;
      }
  }
  .container {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    .card {
      text-align: center;
      @media (max-width: 640px) {
        padding-top: 40px;
      }
      img {
        display: block;
        margin: 0 auto;
        padding: 15px 0;
        height: 150px;
        @media (max-width: 800px) {
          height: 100px;
        }
      }
      p {
        font-size: 2.5rem;
        padding: 15px 30px;
        color: blue;
        @media (max-width: 800px) {
          font-size: 2rem;
        }
      }
    }
  }
  button {
    display: block;
    margin: 80px auto 140px auto;
    font-size: 3rem;
    background: black;
    color: white;
    height: 80px;
    width: 80%;
    border: 3px solid black;
    border-radius: 6px;
    box-shadow: 4px 2px 10px #85858559;
    @media (max-width: 800px) {
      font-size: 2.4rem;
      height: 50px;
    }
    :hover {
      background: #e9ebee;
      color: black;
      transition: 0.2s;
    }
  }
`;

export default function Contacts(props) {
  return (
    <StyledContacts name="contacts" className="element">
      <h2>Contact Me:</h2>
      <div className="container">
        <a href="tel:+3896435579" className="card">
          <img alt="phone" src={phone} />
          <p>+39 389 6435579</p>
        </a>
        <a href="mailto:giacomobenati@mailbox.org" className="card">
          <img alt="mail" src={mail} />
          <p>giacomobenati@mailbox.org</p>
        </a>
      </div>
      <button onClick={() => scroll.scrollToTop()}>Hire a Geek</button>
    </StyledContacts>
  );
}
