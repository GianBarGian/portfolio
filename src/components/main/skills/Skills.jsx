import React from 'react';
import styled from 'styled-components';
import Scroll from 'react-scroll'


import SkillCard from './SkillCard'
import invader1 from '../../../assets/invader5.png'
import invader2 from '../../../assets/invader6.png'
import languages from '../../../states/languages'
import frontend from '../../../states/frontend'
import backend from '../../../states/backend'
import otherSkills from '../../../states/otherSkills'
const StyledSkills = styled.section`
    padding-top: 50px;
    margin: 80px 0 0;
    h2 {
        border-top: 1px solid gray;
        padding: 100px 0 80px 0;
        text-align: center
    }
    .container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }
    .more {
        padding: 40px 0 80px 0;
        text-align: center;
        border-bottom: 1px solid gray;
    }
`

const Skills = () => {
    return (
        <StyledSkills name='skills' className='element'>
            <h2>My Main Area of Expertise</h2>
            <div className='container'>
                <SkillCard title='Langauges' values={languages} invader={invader1} />
                <SkillCard title='Front End' values={frontend} invader={invader2} />
                <SkillCard title='Back End' values={backend} invader={invader2} />
                <SkillCard title='Other Skills' values={otherSkills} invader={invader1} />
            </div>
            <p className='more'>and many more...</p>
        </StyledSkills>
    )
}

export default Scroll.ScrollElement(Skills);
