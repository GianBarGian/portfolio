import React from "react";
import styled from "styled-components";
const StyledSkill = styled.div`
  padding-top: 30px;
  padding-bottom: 30px;
  width: 48%;
  text-align: center;
  border: 3px solid black;
  margin-bottom: 20px;
  background-color: #ffffffd1;
  border-radius: 10px;
  @media (max-width: 1000px) {
    width: 90%;
  }
  h3 {
    padding: 25px 0 50px 0;
  }
  img {
    height: 70px;
  }
  div {
    padding: 0 8%;
    display: flex;
    justify-content: space-between;
    p {
      padding-bottom: 25px;
      text-align: left;
    }
    img {
      height: 30px;
    }
  }
`;

const SkillCard = ({ title, values, invader }) => {
  return (
    <StyledSkill>
      <img alt="" src={invader} />
      <h3>{title}</h3>
      {values.map((value) => (
        <div key={value.name}>
          <p>{value.name}</p>
          {value.icon && <img alt="logo" src={value.icon} />}
        </div>
      ))}
    </StyledSkill>
  );
};

export default SkillCard;
