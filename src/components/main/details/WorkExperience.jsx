import React from "react";
import styled from "styled-components";

const StyledWorxXp = styled.section``;

export default function WorkExperience(props) {
  return (
    <StyledWorxXp name="work-experience" className="element">
      <h2>Work Experience</h2>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://yogicyantra.com"
        className="card"
      >
        <h5>YogicYantra.com</h5>
        <div className="bottom">
          <div>
            <p className="date"> 2020 2022</p>
            <h6>Full Stack Developer</h6>
          </div>
          <p className="text">
            Built and maintained the web app. I designed and built the
            front-end, the back-end and managed all the DevOps. I used a wide
            variety of tools and languages.
          </p>
        </div>
      </a>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://guidemadrelinguafirenze.com/"
        className="card"
      >
        <h5>AGMF</h5>
        <div className="bottom">
          <div>
            <p className="date"> 2021</p>
            <h6>Full Stack Developer</h6>
          </div>
          <p className="text">
            Built and maintained this small web app as a freelancer. I designed
            and built the front-end and managed all the DevOps.
          </p>
        </div>
      </a>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.bloomtech.com/"
        className="card"
      >
        <h5>Bloom Tech</h5>
        <div className="bottom">
          <div>
            <p className="date">2019</p>
            <h6>Team Leader</h6>
          </div>
          <p className="text">
            Managed a team of CS students using Agile Methodologies. My tasks
            were organize stand-up meeting, doing code reviews, approve and
            merge pull-requests.
          </p>
        </div>
      </a>
    </StyledWorxXp>
  );
}
