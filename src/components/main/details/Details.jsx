import React from "react";
import styled from "styled-components";

import WorkExperience from "./WorkExperience";
import Education from "./Education";

const StyledDetails = styled.section`
  h2 {
    padding: 80px 0;
    text-align: center;
  }
  .outside-container {
    display: flex;
    justify-content: space-between;

    .inside-container {
      display: flex;
      justify-content: space-around;
      width: 100%;
      @media (max-width: 1400px) {
        flex-direction: column;
        align-items: center;
      }
      a {
        display: block;
        color: black;
      }
      .card {
        text-align: center;
        border: 3px solid black;
        background-color: #ffffffd1;
        border-radius: 10px;
        height: 240px;
        width: 450px;
        margin-bottom: 20px;
        @media (max-width: 500px) {
          width: 350px;
          height: unset;
          padding-bottom: 19px;
        }
        h5 {
          padding-top: 30px;
          font-size: 2.2rem;
        }
        .bottom {
          display: flex;
          justify-content: space-between;
          align-items: baseline;
          h6 {
            padding-left: 20px;
            text-align: left;
          }
          p {
            padding: 20px;
            text-align: left;
          }
          .date {
            line-height: 1;
          }
          .text {
            font-size: 1.8rem;
          }
        }
      }
    }
  }
`;

export default function Details(props) {
  return (
    <StyledDetails>
      <div className="outside-container">
        <div className="inside-container">
          <Education />
          <WorkExperience />
        </div>
      </div>
    </StyledDetails>
  );
}
