import React from "react";
import styled from "styled-components";

const StyledEducation = styled.section``;

export default function Education(props) {
  return (
    <StyledEducation name="education" className="element">
      <h2>Education</h2>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.coursera.org/account/accomplishments/specialization/certificate/2D59YLUG8GWQ"
        className="card"
      >
        <h5>Blockchain Specialization</h5>
        <div className="bottom">
          <p className="date">2022</p>
          <p className="text">
            16 weeks course by The University at Buffalo (UB) and The State
            University of New York, about blockchain technologies, Solidity,
            smart contracts and D-App development
          </p>
        </div>
      </a>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.bloomtech.com/"
        className="card"
      >
        <h5>Bloom Tech</h5>
        <div className="bottom">
          <p className="date">2019</p>
          <p className="text">
            A top notch full immersion Web dev and CS school, coding everyday
            all day for almost 1 year. On top of CS subjects it was an optimal
            environment to learn best practices and industry workflows.
          </p>
        </div>
      </a>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.edx.org/cs50"
        className="card"
      >
        <h5>CS50 - Computer Science</h5>
        <div className="bottom">
          <p className="date">2019</p>
          <p className="text">
            An online course from Harvard University taught by David J. Malan,
            CS50x teaches students how to think algorithmically and solve
            problems efficiently.
          </p>
        </div>
      </a>
      <div className="card">
        <h5>Liceo scientifico</h5>
        <div className="bottom">
          <p className="date">2001 2006</p>
          <p className="text">
            Scientific lyceum with P.N.I specialization ('National Plan of
            Computer studies')
          </p>
        </div>
      </div>
    </StyledEducation>
  );
}
