import React from "react";
import styled from "styled-components";

const StyledFooter = styled.section`
  position: relative;
  height: 80px;
  background-color: black;
  p {
    position: absolute;
    bottom: 10px;
    left: 0;
    right: 0;
    text-align: center;
    color: white;
    font-size: 1.6rem;
    @media (max-width: 850px) {
      font-size: 1.1rem;
    }
  }
`;

export default function Footer() {
  return (
    <StyledFooter name='footer' className="element" >
      <p>© Copyright 2022 Giacomo Benati · All rights reserved</p>
    </StyledFooter>
  );
}
