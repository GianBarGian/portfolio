import React from "react";
import styled from "styled-components";
import selfie2 from "../../../assets/selfie2.jpg";

import AboutMeText from "./AboutMeText";

const StyledAboutMe = styled.section`
  padding-bottom: 80px;
  border-bottom: 1px solid gray;
  div {
    display: flex;
    justify-content: space-between;
    img {
      width: 49.5%;
      object-fit: contain;
      @media (max-width: 1500px) {
        width: 90%;
      }
    }
    @media (max-width: 1500px) {
      flex-direction: column;
      align-items: center;
    }
  }
`;

export default function AboutMe() {
  return (
    <StyledAboutMe name='about-me' className="element" >
      <h2>About Me</h2>
      <div>
        <img alt="Giacomo Benati" src={selfie2}></img>
        <AboutMeText />
      </div>
    </StyledAboutMe>
  );
}
