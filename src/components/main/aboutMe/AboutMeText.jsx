import React from "react";
import styled from "styled-components";

const StyledText = styled.div`
  font-size: 2rem;
  line-height: 1.5;
  width: 48%;
  @media (max-width: 1500px) {
    padding-top: 20px;
    width: 90%;
  }
`;

export default function AboutMeText() {
  return (
    <StyledText>
      <p>
        <strong>Coding is like a chess game: all about logic.</strong> <br />{" "}
        <br />
        Since I was a kid I was very passionate about <strong>maths</strong>, I
        spent hours and hours playing every kind of strategic game (like{" "}
        <strong>Chess</strong> and <strong>Go</strong>). I have been an online
        professional poker player for a few years, where I learnt how to deal
        with <strong>complex problems analyzing </strong>it from a{" "}
        <strong>mathematical standpoint</strong>. <br /> <br />I discovered
        coding in 2017 and since then it has been my great love. During 2019 I
        attended{" "}
        <strong>
          <a href="https://lambdaschool.com/" target="blank">
            Lambda School
          </a>
        </strong>{" "}
        which gave me a <strong>solid methodology</strong> and a{" "}
        <strong>professional workflow</strong>. Moreover, during my paid
        internship and project with the school, I deepened my knowledge over the{" "}
        <strong>working flow in big companies</strong>, how to work in a{" "}
        <strong>structured team</strong> with{" "}
        <strong>timelines and accountability</strong> using tools like{" "}
        <strong>Trello</strong>, <strong>Git, Zoom and Slack</strong>. <br />{" "}
        <br />
        After that I became a <strong> professional web developer </strong>,
        working as a freelance and as the main{" "}
        <strong> full stack developer</strong> for{" "}
        <strong>
          <a href="https://yogicyantra.com/" target="blank">
            Yogic Yantra
          </a>
        </strong>{" "}
        <br /> <br />
        <strong>
          These set of skills allow me to solve complex code problems, no
          matters what the specific tools are used.
        </strong>
      </p>
    </StyledText>
  );
}
