import React from "react";
import styled from "styled-components";

const StyledCard = styled.div`
  width: 48%;
  border: 3px solid black;
  margin-bottom: 40px;
  background-color: #ffffffd1;
  border-radius: 0 0 10px 10px;
  position: relative;
  @media (max-width: 1600px) {
    width: 90%;
  }
  img {
    height: fit-content;
    width: 100%;
  }
  h3 {
    margin: 25px auto;
  }
  p {
    padding: 0 15px 100px;
    text-align: justify;
  }
  button {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    text-align: center;

    bottom: 20px;
    border: 3px solid black;
    background-color: white;
    color: black;
    font-size: 1.8rem;
    padding: 15px 0;
    width: 50%;
    border-radius: 6px;
    box-shadow: 4px 2px 10px #85858559;
    :hover {
      color: white;
      background-color: black;
      transition: 0.2s;
    }
  }
`;

export default function ProjectsCard({ project }) {
  const { image, imgAlt, title, description, repo } = project;
  return (
    <StyledCard>
      <img src={image} alt={imgAlt} />
      <h3>{title}</h3>
      <p dangerouslySetInnerHTML={{ __html: description }}></p>
      <a href={repo} target="_blank" rel="noopener noreferrer">
        <button>CHECK IT OUT</button>
      </a>
    </StyledCard>
  );
}
