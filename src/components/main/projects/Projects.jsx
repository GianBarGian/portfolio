import React from "react";
import styled from "styled-components";

import projects from "../../../states/projects";
import ProjectsCard from "./ProjectsCard";

const SyledProjects = styled.section`
  border-bottom: 1px solid gray;
  padding-bottom: 80px;
  h2 {
    padding: 80px 0;
  }
  .cards-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    @media (max-width: 1400px) {
      justify-content: center;
    }
  }
`;

export default function Projects() {
  return (
    <SyledProjects name='projects' className="element">
      <h2>Some of my Projects</h2>
      <div className="cards-container">
        {projects.map((project) => (
          <ProjectsCard key={project.title} project={project} />
        ))}
      </div>
    </SyledProjects>
  );
}
