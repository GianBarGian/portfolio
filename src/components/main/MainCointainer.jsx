import React from "react";
import styled from "styled-components";
import { disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';

import Header from "./header/Header";
import AboutMe from "./aboutMe/AboutMe";
import Skills from "./skills/Skills";
import Projects from "./projects/Projects";
import Contacts from "./contacts/Contacts";
import Details from "./details/Details";

import * as faSolid from "styled-icons/fa-solid";
import Footer from "./footer/Footer";

const StyledMain = styled.div`
  margin-left: 300px;
  height: 100%;
  background-color: #cad9ff;
  .menu {
    display: ${(props) => (props.menuShown ? "none" : "block")};
    width: 30px;
    position: fixed;
    top: 20px;
    left: 5%;
    :hover {
      transform: scale(1.1);
      transition: 0.2s;
    }
    @media (max-width: 1150px) {
      display: block;
    }
  }
  @media (max-width: 1150px) {
    margin-left: 0;
  }
`;

const InnerContainer = styled.div`
  width: 90%;
  margin: 0 auto;
  font-size: 1.6rem;
  h2 {
    padding: 80px 0;
    font-size: 4.5rem;
    text-align: center;
    @media (max-width: 1200px) {
      font-size: 3.4rem;
    }
    @media (max-width: 850px) {
      font-size: 2.8rem;
    }
  }
  h3 {
    font-size: 4rem;
    text-align: center;
    @media (max-width: 1200px) {
      font-size: 3rem;
    }
    @media (max-width: 850px) {
      font-size: 2.5rem;
    }
  }
  p {
    font-size: 2.1rem;
    line-height: 1.5;
    @media (max-width: 1200px) {
      font-size: 2rem;
    }
    @media (max-width: 850px) {
      font-size: 1.6rem;
    }
  }
`;

export default function MainContainer({ menuShown, toogleMenu }) {
  menuShown ? disableBodyScroll(document) : enableBodyScroll(document);
  return (
    <StyledMain>
      <Header />
      <InnerContainer>
        <Skills />
        <AboutMe />
        <Projects />
        <Details />
        <Contacts />
      </InnerContainer>
      <Footer />
      <faSolid.Bars
        className="menu"
        menuShown={menuShown}
        onClick={toogleMenu}
      />
    </StyledMain>
  );
}
