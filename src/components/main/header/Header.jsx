import React from 'react';
import styled from 'styled-components';
import Typed from 'react-typed';

import bearded from '../../../assets/bearded.png'
import words from '../../../states/headerAnim'

const StyledHeader = styled.section`
    display:flex;
    justify-content: space-around;
    @media (max-width: 1000px) {
        margin-top: 30px;
    }
    @media (max-width: 885px) {
        flex-direction: column;
        justify-content: start;
        height: auto;
        align-items: center;
    }
    div {
        width: 60%;
        background-image: url(${bearded});
        background-repeat: no-repeat;
        background-position: center top;
        background-size: contain;
        text-align: center;
        font-size: 4.5rem;
        height: fit-content;

        @media (max-width: 1450px) {
            background-size: cover;
            font-size: 4rem;
        }
        @media (max-width: 885px) {
            width: 100%;
            background-size: contain;
            background-position: center;
        }
        @media (max-width: 550px) {
            font-size: 4rem;
        }
        h1 {
            padding-top: 50px;
            color: red;
        }
        h2 {
            padding-top: 500px;
            color: white;
            @media (max-width: 1150px) {
                padding-top: 450px;
            }
            @media (max-width: 550px) {
                padding-top: 250px;
            }
        }
        .typed {
            color: red;
        }
    }
    form {
        margin-top: 20px;
        margin-right: 15px;
        width: 500px;
        background: #ffffffd1;
        border: 3px solid black;
        border-radius: 4%;
        display: flex;
        flex-direction: column;
        height: 700px;
        @media (max-width: 550px) {
            width: 95%;
            height: 900px;
            margin-right: 0;
        }
        h2 {
            font-size: 2.3rem;
            padding: 20px;
            padding-top: 40px;
            line-height: 1.5;
        }
        input {
            padding-left: 10px;
            font-size: 1.8rem;
            margin:  20px;
            height: 60px;
            ::placeholder {
                font-family: 'Futura'
            }
        }
        textarea {
            padding-left: 10px;
            padding-top: 10px;
            line-height: 1.5;
            font-size: 1.8rem;
            margin: 20px;
            height: 200px;
            ::placeholder {
                font-family: 'Futura'
            }
        }
        button {
            font-size: 2.7rem;
            background:black;
            color: white;
            margin: 15px 20px;
            height: 80px;
            border: 3px solid black;
            border-radius: 6px;
            box-shadow: 4px 2px 10px #85858559;
            :hover {
                background: white;
                color: black;
                transition: 0.2s;
            }
        }
        h3 {
            padding: 20px;
            color: red;
            line-height: 1.5;
            font-size: 1.3rem;
        }
        
    }
`

export default function Header() {
    return (
        <StyledHeader name='header' className='element'>
            <div>
                <h1>'Got IT challenges?</h1>
                <h2>Get a Real Geek!</h2>
                <Typed
                    className='typed'
                    strings={words}
                    typeSpeed={60}
                    backSpeed={20}
                    backDelay={1500}
                    smartBackspace={false}
                    loop >
                </Typed>
            </div>
            <form action="https://submit-form.com/sbTFweqrlFxQJ7KGrjIox" >
                <h2>No matter what your problem is, I can solve it. Get in touch!</h2>
                <input type="text" name='name' placeholder='Your Name Here' />
                <input type="text" name='company' placeholder='Your Company Here' />
                <input type="email" name='mail' placeholder='Your Email Here' />
                <textarea name='message' placeholder='Give me details about the job or whatever you like...' />
                <button type="submit">Hire a Geek</button>
                <h3>HEADS UP: I don't collect or use any personal information, apart for getting in touch with you and help you solve your problem!</h3>
            </form>
        </StyledHeader>
    )
}