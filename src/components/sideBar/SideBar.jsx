import React from "react";
import styled from "styled-components";
import { Link } from "react-scroll";

import * as faBrands from "styled-icons/fa-brands";
import * as boxicons from "styled-icons/boxicons-regular";

import SelfieMio from "../../assets/selfiemio.jpg";

const StyledSidebar = styled.div`
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  width: 300px;
  background-color: #1f2831;
  overflow-x: hidden;
  height: ${props => `${props.height +100}px`};
  border-right: 3px solid black;
  @media (max-width: 1150px) {
    width: ${(props) => `${props.width}px`};
  }
  img {
    display: block;
    width: 70%;
    border-radius: 50%;
    margin: 50px auto;
    @media (max-width: 1150px) {
      margin: 10px auto;
      width: 100px;
    }
  }

  h1 {
    text-align: center;
    font-size: 2.4rem;
    font-weight: bold;
    padding-bottom: 15px;
    color: white;
    @media (max-width: 1150px) {
      font-size: 2rem;
      padding-bottom: 10px;
    }
  }

  h3 {
    text-align: center;
    font-size: 1.15rem;
    padding-bottom: 40px;
    color: wheat;
    @media (max-width: 1150px) {
      padding-bottom: 25px;
    }
  }

  li {
    text-align: center;
    font-size: 1.5rem;
    padding: 10px 0;
  }
  a {
    width: 12%;
  }
  button {
    height: 30px;
    font-size: 1.4rem;
    width: 150px;
    cursor: pointer;
    @media (max-width: 1150px) {
      height: 25px;
    }
  }
  .close {
    display: none;
    left: 85%;
    top: 20px;
    position: relative;
    font-size: 2rem;
    color: red;
    width: 35px;
    @media (max-width: 1150px) {
      display: block;
    }
  }
  .logo {
    color: #e2e2e2;
    @media (max-width: 1150px) {
      width: 30px;
    }
  }
  @media (max-width: 1150px) {
    transform: ${(props) =>
      props.menuShown ? "translate(0, 0)" : `translate(-${props.width}px, 0)`};
    transition: 0.5s;
  }
`;

const StyledContacts = styled.div`
  padding-top: 80px;
  display: flex;
  justify-content: space-evenly;
  color: #242222;
`;

export default function SideBar({ menuShown, toogleMenu }) {
  return (
    <StyledSidebar menuShown={menuShown} width={window.innerWidth} height={window.innerHeight}>
      <button className="close" onClick={toogleMenu}>
        X
      </button>
      <div>
        <img src={SelfieMio} alt="Giacomo Benati" />
        <h1>Giacomo Benati</h1>
        <h3>FULL STACK WEB DEV</h3>
        <ul>
          <li>
            <Link onClick={toogleMenu} to="header" smooth={true}>
              <button>Home</button>
            </Link>
          </li>
          <li>
            <Link onClick={toogleMenu} to="skills" smooth={true} offset={40}>
              <button>Skills</button>
            </Link>
          </li>
          <li>
            <Link onClick={toogleMenu} to="about-me" smooth={true} offset={40}>
              <button>About Me</button>
            </Link>
          </li>
          <li>
            <Link onClick={toogleMenu} to="projects" smooth={true} offset={40}>
              <button>Projects</button>
            </Link>
          </li>
          <li className="details">
            <Link
              onClick={toogleMenu}
              to="education"
              smooth={true}
              offset={-10}
            >
              <button>Education</button>
            </Link>
          </li>
          <li className="details">
            <Link
              onClick={toogleMenu}
              to="work-experience"
              smooth={true}
              offset={-10}
            >
              <button>Work Experience</button>
            </Link>
          </li>
          <li>
            <Link onClick={toogleMenu} to="contacts" smooth={true} offset={40}>
              <button>Contact</button>
            </Link>
          </li>
        </ul>
        <StyledContacts>
          <a
            className="logo"
            href="https://gitlab.com/GianBarGian"
            target="_blank"
            rel="noopener noreferrer"
          >
            <faBrands.Gitlab />
          </a>
          <a
            className="logo"
            href="https://www.linkedin.com/in/giacomobenati/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <faBrands.Linkedin />
          </a>
          <a
            className="logo"
            href="mailto:giacomobenati@mailbox.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <boxicons.Envelope />
          </a>
        </StyledContacts>
      </div>
    </StyledSidebar>
  );
}
