## MY PORTFOLIO website

This is my personal Portfolio Website. It's just a little show-off front-end project, built (unnecessarily) in React + Redux. 

You can find the live version [here](https://gianbargian.gitlab.io/portfolio/)